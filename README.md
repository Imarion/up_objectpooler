Instructions
============

Create a pooler object.
Add an ObjectPoolHandler component.
Setup the list of objects to pool. For each element of the list set an prefab to pool and the initial amount of instances to create at startup.
In your "pooler" object, get a reference to the "ObjectPoolHandler" and simply call "Get" and activate it:
	GameObject go = PoolHandler.Get(ObjectToPool);
	go.transform.position = transform.position;
	go.SetActive(true);

To return the prefab to the pool you can call Returntopool on the "ObjectPoolHandler" or simply deactivate it.
In the demo scene the pooled object starts a coroutine to automatically deactivate itself after a set amount of time.

In the demo scene press 'a' key to summon an object in the "Pooler".