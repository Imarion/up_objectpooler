﻿
/* This component is responsible of the pools */

using System.Collections.Generic;
using UnityEngine;


namespace com.tools.objectpooler
{

    public class ObjectPoolHandler : MonoBehaviour, IHandlePools
    {

        [SerializeField] private List<ObjectPool> pools = new List<ObjectPool>();
        private readonly Dictionary<GameObject, IPool> poolLookup = new Dictionary<GameObject, IPool>();

        void Awake()
        {

            foreach (var pool in pools)
            {
                if (poolLookup.ContainsKey(pool.PrefabToPool))
                {
                    Debug.LogError("Same object in multiple pools " + pool.PrefabToPool.name);
                    continue;
                }

                poolLookup.Add(pool.PrefabToPool, pool);

                GameObject parent = new GameObject("Pool: " + pool.PrefabToPool.name);
                parent.transform.parent = transform;
                pool.InitializePool(parent.transform);
            }
        }
        
        public GameObject Get(GameObject prefab)
        {
            if (!poolLookup.ContainsKey(prefab))
            {
                Debug.LogError(prefab.name + " not in pool : " + prefab.GetHashCode());
                return null;
            }
            return poolLookup[prefab].Get();
        }

        public void ReturnToPool(GameObject objectToReturn)
        {
            objectToReturn.SetActive(false);
        }


#if UNITY_EDITOR
    private void OnDestroy()
    {
        foreach (var pool in pools)
        {
            if (pool.InitialAmountToPool < pool.CurrentPoolCount())
            {
                Debug.LogWarning(string.Format("Pool: {0} has more items now ({2}) than when created ({1}) - consider increasing start size",
                    pool.PrefabToPool.name, pool.InitialAmountToPool, pool.CurrentPoolCount()));
            }
        }
    }
#endif

    }


}