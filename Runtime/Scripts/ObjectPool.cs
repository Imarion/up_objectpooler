﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace com.tools.objectpooler
{ 
    [System.Serializable]
    public class ObjectPool : IPool
    {
        [SerializeField] private GameObject prefabToPool = null;
        [SerializeField] int initialAmountToPool = 20;

        public int InitialAmountToPool { get => initialAmountToPool;  set => initialAmountToPool = value;  }
        public GameObject PrefabToPool { get => prefabToPool; set => prefabToPool = value; }

        private Queue<GameObject> pool = new Queue<GameObject>();
        private Transform parent;

        public void InitializePool(Transform thisParent)
        {
            parent = thisParent;
            InitialAmountToPool = initialAmountToPool;
            AddObjects(InitialAmountToPool);
        }

        public GameObject Get()
        {
            GameObject go = null;

            if ((pool.Count > 0) && (!pool.Peek().activeSelf))
            {
                go = pool.Dequeue();
                pool.Enqueue(go);
            }
            else
            {
                go = AddObject();
            }

            return go;
        }

        private GameObject AddObject()
        {
            GameObject newObject = GameObject.Instantiate(PrefabToPool, parent);
            newObject.gameObject.SetActive(false);
            pool.Enqueue(newObject);

            return newObject;
        }

        private void AddObjects(int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                GameObject newObject = GameObject.Instantiate(PrefabToPool, parent);
                newObject.gameObject.SetActive(false);
                pool.Enqueue(newObject);
            }
        }

        public int CurrentPoolCount()
        {
            return pool.Count;
        }
    }
}