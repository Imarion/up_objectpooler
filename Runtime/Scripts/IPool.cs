﻿
using UnityEngine;


namespace com.tools.objectpooler
{
    public interface IPool
    {
        GameObject PrefabToPool { get; set; }
        int InitialAmountToPool { get; set; }

        public GameObject Get();
        public void InitializePool(Transform thisParent);
        public int CurrentPoolCount();
    }
}