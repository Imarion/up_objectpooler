
using UnityEngine;


namespace com.tools.objectpooler
{
    public interface IHandlePools
    {
        public GameObject Get(GameObject prefab);
        public void ReturnToPool(GameObject objectToReturn);
    }
}