using System.Collections;

using UnityEngine;

namespace com.tools.objectpooler_demo
{
    public class PooledObject : MonoBehaviour
    {
        [SerializeField] private float Lifetime = 1.0f;
        private WaitForSeconds AutoDeactivateWFS = null;

        private void Awake()
        {
            AutoDeactivateWFS = new WaitForSeconds(Lifetime);
        }

        private void OnEnable()
        {
            StartCoroutine(AutoDeactivate());
        }

        private IEnumerator AutoDeactivate()
        {
            yield return AutoDeactivateWFS;
            gameObject.SetActive(false);
        }
    }

}
