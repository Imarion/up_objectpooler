
using UnityEngine;
using com.tools.objectpooler;

namespace com.tools.objectpooler_demo
{
    public class Pooler : MonoBehaviour
    {

        private IHandlePools PoolHandler;
        [SerializeField] private GameObject ObjectToPool = null;

        void Awake()
        {
            PoolHandler = GetComponent<IHandlePools>();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                GameObject go = PoolHandler.Get(ObjectToPool);
                go.transform.position = transform.position;
                go.SetActive(true);
            }
        }
    }
}
